:- module(sort_setting).

:- use_module(library(cio)).
:- dynamic sort_setting/2.
:- export set_setting/2,
      set_settings/1,
      set_settings_java/0,
      get_settings_java/0,
      sort_setting/2.
:- ["sort_settings.ecl"].
/**
 * set_sc(:Parameter:atom,+Value:term) is det
 *
 * The predicate sets the value of a parameter
 * For a list of parameters see
 * https://github.com/friguzzi/cplint/blob/master/doc/manual.pdf or
 * http://ds.ing.unife.it/~friguzzi/software/cplint-swi/manual.html
 */
set_setting(Parameter,Value):-
  retractall(sort_setting(Parameter,_)),
  assert(sort_setting(Parameter,Value)).

set_settings([]).
set_settings(Settings) :-
  writeln("set_settings"),
  (
    foreach(Setting, Settings)
  do
    writeln(Setting),
    Setting = sort_setting(Parameter, Value),
    writeln("qui"),
    set_setting(Parameter, Value)
  ).

set_settings_java :-
  writeln("set_settings_java"),
  read_exdr(java_to_eclipse, OutputFile),
  write("output file: "), writeln(OutputFile),
  read_exdr(java_to_eclipse, Settings),
  writeln("set_settings_java"),
  writeln(Settings),
  writeln("set_settings_java"),
  flush(output),
  set_settings(Settings),
  atom_string(OutputFileAtom, OutputFile),
  tell(OutputFileAtom),
  listing(sort_setting/2),
  told,
  writeln("wrote file"),
  flush(output),
  write_exdr(eclipse_to_java, "ok"),
  flush(eclipse_to_java).

get_settings_java :-
  findall(sort_setting(K,V), sort_setting(K,V), Settings),
  write_exdr(eclipse_to_java, Settings),
  flush(eclipse_to_java).
