/*
* Prolog Source File engine.ecl
*   created ${date} at ${time}
*   created by Giuseppe Cota email:giuseppe.cota@unife.it
*/
:- module(sort).
:- export
    solve/4,
    % solve/2,
    solve/0,
    read_db_data/0,
    read_boxes/0,
    read_orders/0.


:- use_module(library(lists)).
:- use_module(library(listut)).
:- use_module(library(viewable)).
:- use_module(sort_setting).
:- use_module(utils).
:- use_module(library(util)).

:- use_module(data).
:- use_module(logger).

:- use_module(clp_engine).
% :- use_module(clp_engine2).
:- use_module(milp_engine).

read_db_data :-
  catch(
    read_db_data_int,
    Problem,
    (
      printf("Execution of read_db_data_int/0 aborted with %w%n", [Problem]),
      write_exdr(eclipse_to_java, Problem)
    )
  ).

read_db_data_int :-
  writeln("set db data"),
  read_exdr(java_to_eclipse, OutputFile),
  % assertz(db_file(OutputFile)),
  write("output file: "),
  writeln(OutputFile),
  writeln("read_db_data"),
  read_exdr(java_to_eclipse, Data),
  save_data(Data, OutputFile),
  write_exdr(eclipse_to_java, "ok"),
  flush(eclipse_to_java).

% reads the list of boxes that must be opened
read_boxes :-
  writeln("read_boxes"),
  read_exdr(java_to_eclipse, Boxes),
  set_boxes_to_use(Boxes),
  writeln("Boxes to use:"),
  listing(box_to_use/1) @ data,
  write_exdr(eclipse_to_java, "ok"),
  flush(eclipse_to_java).

% reads the list of orders that must be satisfied
read_orders :-
  writeln("read_orders"),
  read_exdr(java_to_eclipse, Orders),
  set_orders_to_satisfy(Orders),
  writeln("Orders to satisfy:"),
  listing(order_to_satisfy/1) @ data,
  write_exdr(eclipse_to_java, "ok"),
  flush(eclipse_to_java).

solve :-
  catch(
    solve_int,
    Problem,
    (
      printf("Cannot find solution %w%n", [Problem]),
      write_exdr(eclipse_to_java, Problem)
    )
  ), !.

solve :-
  write_exdr(eclipse_to_java, "NO").


solve_int :-
  writeln("reading DB file path"),
  read_exdr(java_to_eclipse, DBFile),
  write("output file: "),
  writeln(DBFile),
  %assertz(db_file(DBFile)),
  writeln("solving..."),
  solve(_ArticleVars, _OrderVars, _Profit, DBFile).


% solve(ArticleVars, OrderVars) :-
%     % AllowedMethods = [complete,lds(2),credit(64,5),bbs(1000),dbs(5,10)],
%     % AllowedSelects = [first_fail,most_constrained,input_order],
%     % AllowedChoices = [indomain,
%     %                indomain_min,
%     % 	       indomain_max,
%     % 	       indomain_middle,
%     % 	       indomain_median,
%     % 	       indomain_split,
%     % 	       indomain_random,
%     %          gfd_search:indomain(_)],
%     % AllowedStrategies = [continue, dichotomic],
%     % member(MethodArticleVars, AllowedMethods),
%     % member(MethodOrderVars, AllowedMethods),
%     % member(SelectArticleVars,),
%     % member(Strategy, AllowedStrategies),
%     % member(Choice,),
%     % clean database
%     %clean_db,
%     %writeln("cleaning completed"),
%     % load dataset
%     db_file(DBFile),
%     solve(ArticleVars, OrderVars, DBFile).

solve(ArticleVars, OrderVars, Profit, Dataset) :-
    logger:debugln("cleaning previous data"),
    clean_db,
    logger:debugln("cleaning completed"),
    logger:traceln("Orders to satisfy:"),
    logger:trace(listing(order_to_satisfy/1) @ data),
    logger:traceln("Boxes to use:"),
    logger:trace(listing(box_to_use/1) @ data),
    % get all the search settings
    sort_setting(method_articles, MethodArticleVars),
    sort_setting(select_articles, SelectArticleVars),
    sort_setting(choice_articles, ChoiceArticleVars),
    sort_setting(method_orders, MethodOrderVars),
    sort_setting(select_orders, SelectOrderVars),
    sort_setting(choice_orders, ChoiceOrderVars),
    sort_setting(strategy, Strategy),
    sort_setting(search_style, SearchStyle),
    sort_setting(delta, Delta),
    sort_setting(timeout, Timeout),
    sort_setting(solver, Solver),
    logger:debugln("read all settings"),
    logger:debugln("reading dataset"),
    load_db(Dataset),
    logger:debugln("loaded dataset"),
    logger:traceln("Food articles: "),
    logger:trace(listing(article/7) @ data),
    logger:traceln("Orders: "),
    logger:trace(listing(order/5) @ data),
    logger:traceln("Items: "),
    logger:trace(listing(item/4) @ data),
    member(SearchStyle, [together, separated]),
    % get article ids
    findall(ArticleId, article{article_id:ArticleId}, ArticleIds),
    findall(Quantity, article{quantity:Quantity}, Quantities),
    Q is sum(Quantities),
    writeln(Q),
    % get order Ids
    findall(OrderId, order{order_id:OrderId}, OrderIds),
    % get box ids
    findall(BoxId, article{box_id:BoxId}, BoxIdsRepeated),
    % removes duplicates
    sort(BoxIdsRepeated, BoxIds),
    % define variables
    length(ArticleIds, NArticles),
    length(OrderIds, NOrders),
    length(BoxIds, NBoxes),
    Solver:define_vars(NArticles, NOrders, NBoxes, ArticleVars, ArticleBoolVars, OrderVars, BoxVars),!,
    %
    % infoln(solve(NArticles, method_articles:MethodArticleVars,
    %   select_articles:SelectArticleVars, choice_articles:ChoiceArticleVars,
    %   method_orders:MethodOrderVars, select_orders:SelectOrderVars,
    %   choice_orders:ChoiceOrderVars, strategy:Strategy, delta:Delta, timeout:Timeout)),
    logger:debugln("defined variables"),
    % setup constraints
    user_constraints(ArticleIds, ArticleVars, OrderIds, OrderVars),!,
    logger:debugln("defined user constraints"),
    Solver:set_constraints(ArticleIds, ArticleVars, ArticleBoolVars, OrderIds, OrderVars, BoxIds, BoxVars),
    logger:debugln("defined system constraints"),!,
    viewable_create(orders, OrderVars),
    viewable_create(articles, ArticleVars),
    viewable_create(article_bools, ArticleBoolVars),
    % viewable_create(profit, [Profit]),
    Solver:define_profit(ArticleIds, ArticleBoolVars, ArticleVars, OrderIds, OrderVars, ProfitObj),
    logger:debugln("defined profit"),
    logger:debugln(ProfitObj),
    logger:debugln("Starting search..."),
    % flush(output),
    Solver:search(ArticleIds, ArticleVars, OrderIds, OrderVars, ProfitObj, Profit),
    %write_term(ArticleVars, [depth(full), nl(true)]),
    %write_term(OrderVars, [depth(full), nl(true)]),
    true.

user_constraints(ArticleIds, ArticleVars, OrderIds, OrderVars) :-
  sort_setting(solver, Solver),
  Solver:box_user_constraints(ArticleIds, ArticleVars),
  Solver:order_user_constraints(ArticleIds, ArticleVars, OrderIds, OrderVars).
