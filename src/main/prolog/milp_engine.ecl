:- module(milp_engine).

%:- use_module(library(eplex)).
:- use_module(library(eplex_osi_clpcbc)).
%:- use_module(library(eplex_osi_glpk)).
%:- use_module(library(eplex_osi_symclp)).
%:- use_module(library(eplex_gurobi)).
:- use_module(library(listut)).
:- use_module(data).
:- use_module(utils).
:- use_module(sort_setting).
:- use_module(logger).
:- use_module(library(matrix_util)).


:- export
    define_vars/7,
    box_user_constraints/2,
    order_user_constraints/4,
    set_constraints/7,
    define_profit/6,
    search/6.

:- eplex_instance(dss). % declare an eplex instance

define_vars(NArticles, NOrders, NBoxes, ArticleVars, [], OrderVars, BoxVars) :-
  define_article_vars(NArticles, NOrders, ArticleVars),
  define_order_vars(NOrders, OrderVars),
  define_box_vars(NBoxes, BoxVars).

define_article_vars(NArticles, NOrders, ArticleVars) :-
  NRows = NArticles,
  % coulumns = number of orders + column stay + column go back
  NColumns is NOrders + 2,
  dim(ArticleVars, [NRows, NColumns]),
  dss: (ArticleVars $:: 0..1),
  dss: integers(ArticleVars).


% create the list of order variables
define_order_vars(NOrders, OrderVars) :-
  length(OrderVars, NOrders),
  dss: (OrderVars $:: 0..1),
  dss: integers(OrderVars).

define_box_vars(NBoxes, BoxVars) :-
  length(BoxVars, NBoxes),
  dss: (BoxVars $:: 0..1),
  dss: integers(BoxVars).

box_user_constraints(ArticleIds, Vars) :-
  sort_setting(stay_column, StayColumn),
  findall(BoxId, box_to_use(BoxId), BoxIds),
  length(BoxIds, L),
  dim(Vars, [NRows, _NColumns]),
  (L > 0 ->
    (
      foreach(ArticleId, ArticleIds),
      for(I, 1, NRows),
      param(BoxIds, Vars, StayColumn)
    do
      article{article_id:ArticleId, box_id:BoxId},
      (member(BoxId, BoxIds) ->
        dss: (Vars[I, StayColumn] $= 0)
      ;
        % do nothing
        true
      )
    )
  ;
    % do nothing
    true
  ).

order_user_constraints(_ArticleIds, ArticleVars, OrderIds, OrderVars) :-
  findall(_OrderId, order_to_satisfy(_OrderId), OrderToSatisfyIds),
  dim(ArticleVars, [_NRows, NColumns]),
  sort_setting(go_back_column, GoBackColumn),
  sort_setting(stay_column, StayColumn),
  length(OrderToSatisfyIds, L),
  (L > 0 ->
    (
      for(J, 1, NColumns),
      param(OrderToSatisfyIds, GoBackColumn, StayColumn, ArticleVars, OrderIds, OrderVars)
    do
      (J \= GoBackColumn, J \= StayColumn ->
        J2 is J - 2,
        nth1(J2, OrderIds, OrderId),
        nth1(J2, OrderVars, OrderVar),
        (member(OrderId, OrderToSatisfyIds) ->
          dss: (sum(ArticleVars[*, J]) $>= 1),
          dss: (OrderVar $= 1)
        ;
          true
        )
      ;
        true
      )
    )
  ;
    % do nothing
    true
  ).

set_constraints(ArticleIds, ArticleVars, _, OrderIds, OrderVars, BoxIds, BoxVars) :-
  article_constraints(ArticleIds, OrderIds, ArticleVars, OrderVars, BoxIds, BoxVars),
  order_constraints(ArticleIds, OrderIds, ArticleVars, OrderVars).

% row constraints
row_constraints(ArticleVars) :-
  dim(ArticleVars, [R,_C]),
  (
    for(I,1,R),
    param(ArticleVars)
  do
    dss: (sum(ArticleVars[I, *]) $= 1)
  ).

article_constraints(ArticleIds, OrderIds, ArticleVars, OrderVars, BoxIds, BoxVars) :-
  row_constraints(ArticleVars),
  incompatibility_constraint(ArticleIds, OrderIds, ArticleVars),
  box_consistency_constraint(ArticleIds, ArticleVars, BoxIds, BoxVars),
  box_stay_constraint(ArticleIds, ArticleVars, BoxIds),
  article_order_constraint(ArticleVars, OrderVars).

order_constraints(ArticleIds, OrderIds, ArticleVars, OrderVars) :-
  unsatisfiable_orders_constraint(OrderIds, OrderVars, ArticleVars),
  % there is a limit of satisfiable orders. This limit is the number of Sorter's outputs
  max_satisfiable_orders(OrderVars),
  % constraint between orders with the same feed category.
  % The following constraint was commented out because on average it does not improve the performances
  % competitive_orders_constraints(ArticleIds, ArticleVars, OrderIds, OrderVars),
  % for each order of the same category, if all the compatible articles are associated
  % with "stay" or "go back" then all the order of the same category have destination "stay"
  stay_order_constraints(ArticleIds, ArticleVars, OrderIds, OrderVars),
  % satisfiability of the order
  %order_quantity_constraint(ArticleIds, ArticleVars, OrderIds, OrderVars),
  order_quantity_constraint2(ArticleIds, ArticleVars, OrderIds, OrderVars, 1),
  true.

/**
  Articles can’t be associated with incompatible orders
*/
incompatibility_constraint(ArticleIds, OrderIds, Vars) :-
  dim(Vars, [NRows, _NColumns]),
  sort_setting(go_back_column, _GoBackColumn),
  sort_setting(stay_column, _StayColumn),
  (
    foreach(ArticleId, ArticleIds),
    for(I, 1, NRows),
    param(Vars, OrderIds)
  do
    get_incompatible_orders_by_article(ArticleId, IncompatibleOrderIds),
    (
      foreach(OrderId, IncompatibleOrderIds),
      % param(Vars, OrderIds, I, GoBackColumn, StayColumn, IncompatibleOrderIds)
      param(Vars, OrderIds, I)
    do
      nth1(OIdx, OrderIds, OrderId),
      J is OIdx + 2,
      dss: (Vars[I, J] $= 0)
    )
  ).

box_consistency_constraint(ArticleIds, Vars, BoxIds, BoxVars) :-
  dim(Vars, [NRows, NCols]),
  sort_setting(stay_column, StayCol),
  % setof(BoxId,
  % Id^EAN^E^SD^FC^Q^article{article_id:Id, ean:EAN, box_id:BoxId, expiry:E, storage_date:SD, feed_category:FC, quantity:Q},
  % BoxIds),
  % length(BoxIds, NBoxes),
  % length(BoxVars, NBoxes),
  % dss: (BoxVars $:: 0..1),
  % dss: integers(BoxVars),
  (
    for(I, 1, NRows),
    foreach(ArticleId, ArticleIds),
    param(BoxIds, Vars, BoxVars, StayCol, NCols)
  do
    article{article_id:ArticleId, box_id:BoxId},
    get_var_by_id(BoxId, BoxIds, BoxVars, BoxVar),
    % if at least one element in the row is greater than 0 (except for the column stay)
    % then the box that contains the I-th article must be 1
    (
      for(J, 1, NCols),
      param(StayCol, I, Vars, BoxVar)
    do
      (J \= StayCol ->
        dss: (Vars[I, J] $=< BoxVar)
      ;
        true
      )
    )
  ),
  %
  findall(
    SameBoxArticleIds,
    bagof(
      ArticleId,
      EAN^E^SD^FC^Q^article{article_id:ArticleId, ean:EAN, box_id:_BoxId, expiry:E, storage_date:SD, feed_category:FC, quantity:Q},
      SameBoxArticleIds),
    ListOfSameBoxArticleIds
  ),
  (
    foreach(SameBoxArticleIds, ListOfSameBoxArticleIds),
    param(ArticleIds, Vars, BoxIds, BoxVars, StayCol)
  do
    [ArticleId | _] = SameBoxArticleIds, !,
    article{article_id:ArticleId, box_id:BoxId},
    get_var_by_id(BoxId, BoxIds, BoxVars, BoxVar),
    (
      foreach(SameBoxArticleId, SameBoxArticleIds),
      param(ArticleIds, Vars, BoxVar, StayCol)
    do
      nth1(I, ArticleIds, SameBoxArticleId),
      dss: (Vars[I, StayCol] + BoxVar $=< 1)
    )
  ).

box_stay_constraint(ArticleIds, ArticleVars, BoxIds) :-
  sort_setting(go_back_column, GoBackCol),
  subscript(ArticleVars, [*, GoBackCol], ArticlesGoBackCol),
  array_list(ArticlesGoBackCol, ArticlesGoBackList),
  (
    foreach(BoxId, BoxIds),
    param(ArticleIds, ArticlesGoBackList)
  do
    findall(ArticleId, article{article_id:ArticleId, box_id:BoxId}, SameBoxArticleIds),
    length(SameBoxArticleIds, NSameBoxArticles),
    get_articlevars_by_ids(SameBoxArticleIds, ArticleIds, ArticlesGoBackList, SameBoxArticlesGoBackList),
    dss: (sum(SameBoxArticlesGoBackList) $=< NSameBoxArticles - 1)
  ).


article_order_constraint(ArticleVars, OrderVars) :-
  dim(ArticleVars, [NRows, NCols]),
  sort_setting(stay_column, StayCol),
  sort_setting(go_back_column, GoBackCol),
  ( for(I, 1, NRows),
    param(ArticleVars, NCols, StayCol, GoBackCol, OrderVars)
  do
    ( for(J, 1, NCols),
      param(I, ArticleVars, StayCol, GoBackCol, OrderVars)
    do
      (J \= GoBackCol, J \= StayCol ->
        J2 is J - 2,
        nth1(J2, OrderVars, OrderVar),
        dss: (ArticleVars[I,J] $=< OrderVar)
      ;
        true
      )
    )
  ).

unsatisfiable_orders_constraint(OrderIds, OrderVars, ArticleVars) :-
  unsatisfiable_orders(OrderIds, UnsatisfiableOrderIds),
  (
    foreach(UnsatOrderId, UnsatisfiableOrderIds),
    param(OrderVars, OrderIds, ArticleVars)
  do
    get_var_by_id(UnsatOrderId, OrderIds, OrderVars, OrderVar),
    dss: (OrderVar $= 0)
    % nth1(J, OrderIds, UnsatOrderId),
    % J2 is J + 2,
    % dss: (sum(ArticleVars[*, J2]) $= 0)
  ).

/**
  The Sorter has NDest possible destinations. Therefore only NDest orders can be
  satisfied at most.
*/
max_satisfiable_orders(OrderVars) :-
  sort_setting(destinations, NDest),
  dss: (sum(OrderVars) $=< NDest).

% It seems to improve the performances
competitive_orders_constraints(ArticleIds, ArticleVars, OrderIds, OrderVars) :-
  sort_setting(go_back_column, GoBackCol),
  sort_setting(stay_column, StayCol),
  findall(
    Ids,
    bagof(
      Id,
      D^Q^I^order{ order_id:Id, feed_category:_C, deadline:D, quantity:Q, income:I},
      Ids
    ),
    ListOfListOrderIds
  ),
  (
    foreach(CompetitiveOrderIds, ListOfListOrderIds),
    param(OrderVars, OrderIds, ArticleIds, ArticleVars, GoBackCol, StayCol)
  do
    length(CompetitiveOrderIds, NOrders),
    ( NOrders > 1 ->
        get_ordervars_by_ids(CompetitiveOrderIds, OrderIds, OrderVars, CompetitiveOrderVars),
        CompetitiveOrderIds = [OrderId | _],
        get_compatible_articles_by_order(OrderId, CompatibleArticleIds),
        get_article_quantities(CompatibleArticleIds, CompatibleArticleQuantities),
        (
          foreach(CompatibleArticleId, CompatibleArticleIds),
          foreach(ArticleBool, CompatibleArticleBools),
          param(ArticleIds, ArticleVars, GoBackCol, StayCol)
        do
          nth1(I, ArticleIds, CompatibleArticleId),
          dss: (ArticleVars[I, GoBackCol] + ArticleVars[I, StayCol] $= 1 - ArticleBool)
        ),
        get_order_quantities(CompetitiveOrderIds, OrderQuantities),
        dss: (sum(CompatibleArticleQuantities * CompatibleArticleBools) $= AvailableQuantity),
        dss: (sum(OrderQuantities * CompetitiveOrderVars) $=< AvailableQuantity)
      ;
        true
    )
  ).

% TO DO: implement this constraint in order to check if the performances improve
stay_order_constraints(_ArticleIds, _ArticleBools, _OrderIds, _OrderVars) :-
  true.

order_quantity_constraint(ArticleIds, ArticleVars, OrderIds, OrderVars) :-
  % dim(ArticleVars, [NRows, NCols]),
  % sort_setting(stay_column, StayCol),
  % sort_setting(go_back_column, GoBackCol),
  sort_setting(max_order_surplus, Surplus),
  ( foreach(OrderId, OrderIds),
    param(ArticleIds, ArticleVars, OrderIds, OrderVars, Surplus)
  do
    get_var_by_id(OrderId, OrderIds, OrderVars, OrderVar),
    nth1(OrderIdx, OrderIds, OrderId),
    % get the desired quantity of the order
    order{order_id:OrderId, quantity:OrderQuantity},
    % get the list of the article that have the same feed category of the order
    get_compatible_articles_by_order(OrderId, SameCategoryArticleIds),
    findall(
      Idx,
      (member(SameCatArticleId, SameCategoryArticleIds),
      nth1(Idx, ArticleIds, SameCatArticleId)),
      ArticleIndices
    ),
    get_article_quantities(SameCategoryArticleIds, ArticleQuantities),
    MaxAvailableQuantity is sum(ArticleQuantities),
    length(ArticleIndices, NSameCatArticles),
    length(SameCatArticleVars, NSameCatArticles),
    (MaxAvailableQuantity >= OrderQuantity ->
      (
        foreach(Idx, ArticleIndices),
        foreach(SameCatArticleVar, SameCatArticleVars),
        param(ArticleVars, OrderIdx)
      do
        Col is OrderIdx + 2,
        dss: (SameCatArticleVar $= ArticleVars[Idx, Col])
      ),
      MaxOrderQuantity is OrderQuantity + Surplus,
      dss: (sum(ArticleQuantities * SameCatArticleVars) $>= OrderVar * OrderQuantity),
      dss: (sum(ArticleQuantities * SameCatArticleVars) $=< OrderVar * MaxOrderQuantity)
    ;
      true
    )
  ).


order_quantity_constraint2(_, _, [], [], _).
order_quantity_constraint2(Article_ids, Article_vars, [Order_id|Order_ids], [Order_var|Order_vars], Order_idx) :-
   sort_setting(max_order_surplus, Surplus),
   % get the desired quantity of the order
   %order(Order_id, _, _, Order_quantity, _),
   order{order_id:Order_id, quantity:Order_quantity},
   % get the list of the article that have the same feed category of the order
   % Compatible_article_ids unifies with a list of article identifiers
   get_compatible_articles_by_order(Order_id, Compatible_article_ids),
   findall(
     Idx,
     (member(Compatible_article_id, Compatible_article_ids),
     nth1(Idx, Article_ids, Compatible_article_id)),
     Article_indices
   ),
   get_article_quantities(Compatible_article_ids, Article_quantities),
   Available_quantity is sum(Article_quantities),
   dim(Article_vars, [NRows, NColumns]),
   array_flat(1, Article_vars, Article_vars_flat),
   array_list(Article_vars_flat, Article_vars_flat_list),
   list2rows(Article_vars_flat_list, NRows, NColumns, Rows),
   transpose(Rows, T),
   Col is Order_idx + 2,
   nth1(Col, T, Article_order_vars),
   (Available_quantity >= Order_quantity ->
     % Here it is the problem! You cannot use findall here!
     % findall(
     %   V,
     %   (member(Idx2, Article_indices),
     %   nth1(Idx2, Article_order_vars, V)),
     %   Compatible_article_vars2
     % ),
     select_by_indices(Article_indices, Article_order_vars, Compatible_article_vars),
     MaxOrderQuantity is Order_quantity + Surplus,
     dss: (sum(Article_quantities * Compatible_article_vars) $>= Order_var * Order_quantity),
     dss: (sum(Article_quantities * Compatible_article_vars) $=< Order_var * MaxOrderQuantity )
   ;
     dss: (Order_var $= 0)
   ),
   Order_idx_new is Order_idx + 1,
   order_quantity_constraint2(Article_ids, Article_vars, Order_ids, Order_vars, Order_idx_new).

select_by_indices([], _, []).
select_by_indices([Idx|Indices], Vars, [S|T]) :-
  nth1(Idx, Vars, S),
  select_by_indices(Indices, Vars, T).

define_profit(ArticleIds, _, ArticleVars, OrderIds, OrderVars, ProfitObj) :-
  % get order incomes
  get_order_incomes(OrderIds, OrderIncomes),
  dss: (TotalIncome $= sum(OrderIncomes * OrderVars)),
  % sort_setting(destinations, NDestinations),
  % findall(E, (nth1(I,OrderIncomes,E), I =< NDestinations), BestOrderIncomes),
  % Limit is sum(BestOrderIncomes),
  % Profit #=< Limit,
  % writeln(Limit),
  get_article_storage_costs(ArticleIds, ArticleStorageCosts),
  AllArticlesTotalStorageCosts is sum(ArticleStorageCosts),
  penalties(ArticleIds, ArticleVars, OrderIds, OrderVars, TotalPenalty),
  ProfitObj = TotalIncome - TotalPenalty.

penalties(ArticleIds, ArticleVars, OrderIds, OrderVars, TotalPenalty) :-
  today_storage_cost_unused_articles_penalty(ArticleIds, ArticleVars, UnusedArticlesPenalty),
  late_order_penalties(OrderIds, OrderVars, LateOrderPenalty),
  %LateOrderPenalty = 0,
  sorting_penalties(ArticleIds, ArticleVars, SortingPenalty),
  TotalPenalty = UnusedArticlesPenalty + LateOrderPenalty + SortingPenalty.

/**
* Penalty for keeping one more day the unused articles.
*/
today_storage_cost_unused_articles_penalty(ArticleIds, ArticleVars, UnusedArticlesPenalty) :-
  sort_setting(daily_storage_cost, DailyStorageCost),
  sort_setting(go_back_column, GoBackCol),
  sort_setting(stay_column, StayCol),
  dim(ArticleVars, [NArticles, _]),
  % length(ArticleDailyStorageCosts, NArticles),
  % maplist(=(DailyStorageCost), ArticleDailyStorageCosts),
  (
    foreach(ArticleId, ArticleIds),
    foreach(ArticleDailyStorageCost, ArticleDailyStorageCosts),
    param(DailyStorageCost)
  do
    article{article_id:ArticleId, ean:EAN, quantity:ArticleQuantity},
    item{ean:EAN, quantity:ItemQuantity},
    %ArticleDailyStorageCost is integer(DailyStorageCost * round(ArticleQuantity / ItemQuantity))
    ArticleDailyStorageCost is integer(DailyStorageCost * div(ArticleQuantity, 1000))
  ),
  logger:debugln(write_term(ArticleDailyStorageCosts)),
  dss: (UnusedArticlesPenalty $= sum(ArticleDailyStorageCosts * ArticleVars[*,GoBackCol])
    + sum(ArticleDailyStorageCosts * ArticleVars[*,StayCol])).

% /**
% * Penalty for not meeting order deadlines
% */
% late_order_penalties(OrderIds, OrderVars, LateOrderPenalty) :-
%   sort_setting(late_order_penalty, LOP),
%   (
%     foreach(OrderId, OrderIds),
%     foreach(Penalty, Penalties),
%     % TodayLateOrderPenalty is the penalty that must be added if the order is not satisfied today
%     foreach(TodayPenalty, TodayPenalties),
%     param(LOP)
%   do
%     today(Today),
%     order{order_id:OrderId, deadline:Deadline},
%     date_difference(Deadline, Today, Diff),
%     ( Diff =< 0 ->
%       Penalty is (-Diff) * LOP,
%       TodayPenalty is LOP
%     ;
%       Penalty = 0,
%       TodayPenalty = 0
%     )
%   ),
%   LowerBoundLOP is sum(Penalties),
%   % invert order bool variables Bool = 1 iff OrderVar = 0
%   length(OrderVars, NOrders),
%   length(NegOrderVars, NOrders),
%   NegOrderVars $:: 0..1,
%   dss: integers(NegOrderVars),
%   (
%     foreach(OrderVar, OrderVars),
%     foreach(NegOrderBool, NegOrderVars)
%   do
%     dss: (1 - OrderVar $= NegOrderBool)
%   ),
%   sum(TodayPenalties * NegOrderVars) = TodayLateOrderPenalty,
%   LateOrderPenalty = LowerBoundLOP + TodayLateOrderPenalty,
%   true.

/**
* Penalty for not meeting order deadlines
*/
late_order_penalties(OrderIds, OrderVars, LateOrderPenalty) :-
  sort_setting(late_order_penalty, LOP),
  (
    foreach(OrderId, OrderIds),
    foreach(Penalty, Penalties),
    % TodayLateOrderPenalty is the penalty that must be added if the order is not satisfied today
    foreach(TodayPenalty, TodayPenalties),
    param(LOP)
  do
    today(Today),
    order{order_id:OrderId, deadline:Deadline},
    date_difference(Deadline, Today, Diff),
    ( Diff =< 0 ->
      Penalty is (-Diff) * LOP + LOP,
      TodayPenalty is LOP
    ;
      Penalty = 0,
      TodayPenalty = 0
    )
  ),
  % invert order bool variables Bool = 1 iff OrderVar = 0
  length(OrderVars, NOrders),
  length(NegOrderVars, NOrders),
  NegOrderVars $:: 0..1,
  dss: integers(NegOrderVars),
  (
    foreach(OrderVar, OrderVars),
    foreach(NegOrderBool, NegOrderVars)
  do
    dss: (1 - OrderVar $= NegOrderBool)
  ),
  dss: (sum(Penalties * NegOrderVars) $= LateOrderPenalty).

sorting_penalties(ArticleIds, ArticleVars, SortingPenalty) :-
  sort_setting(sorting_cost, SortingCost),
  sort_setting(go_back_column, GoBackCol),
  (
    foreach(ArticleId, ArticleIds),
    foreach(ArticleSortingCost, ArticleSortingCosts),
    param(SortingCost)
  do
    article{article_id:ArticleId, ean:EAN, quantity:ArticleQuantity},
    item{ean:EAN, quantity:ItemQuantity},
    ArticleSortingCost is integer(SortingCost * div(ArticleQuantity, ItemQuantity))
    % iff the destination of an article is -1 then the bool var is 1, these bool
    % are collected into ArticleSortingBools
  ),
  dss: (SortingPenalty $= sum(ArticleSortingCosts * ArticleVars[*,GoBackCol])).
  %SortingPenalty = sum(ArticleSortingCosts * ArticleVars[*,GoBackCol]).

/**
ProfitObj is the profit objective function
*/
search(_ArticleIds, ArticleVars, _OrderIds, OrderVars, ProfitObj, Profit) :-
  %....
  sort_setting(timeout, Timeout),
  dss: eplex_solver_setup(max(ProfitObj), Profit, [timeout(Timeout)], []),
  %%
  dss: eplex_solve(Profit),
  (foreachelem(V, ArticleVars) do
    % set the problem variables to their solution values
    dss: eplex_var_get(V, typed_solution, V)
  ),
  (foreach(V, OrderVars) do
    dss: eplex_var_get(V, typed_solution, V)
  ),
  sort_setting(standalone, Standalone),
  dim(ArticleVars, [NArticles, _]),
  (Standalone ->
    (for(I, 1, NArticles), param(ArticleVars) do
      subscript(ArticleVars, [I, *], Row),
      infoln(Row)
      % write_term(Row, [depth(full), nl(true), flush(true)])
    ),
    infoln(OrderVars),
    infoln(Profit)
    % write_term(OrderVars, [depth(full), nl(true), flush(true)]),
    % write_term(Profit, [depth(full), nl(true), flush(true)])
  ;
    % TO DO: output to java
    true
  )
  .
