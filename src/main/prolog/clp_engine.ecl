:- module(clp_engine).

:- use_module(library(gfd)).
% I must use this library for search if I want to perform optimization.
:- use_module(library(gfd_search)).
:- use_module(library(branch_and_bound)).
:- use_module(library(listut)).
:- use_module(data).
:- use_module(utils).
:- use_module(sort_setting).
:- use_module(logger).


:- export
    define_vars/7,
    box_user_constraints/2,
    order_user_constraints/4,
    set_constraints/7,
    define_profit/6,
    search/6.


define_vars(NArticles, NOrders, _NBoxes, ArticleVars, ArticleBoolVars, OrderVars, []) :-
	define_article_vars(NOrders, NArticles, ArticleVars),
  define_article_bool_vars(ArticleVars, ArticleBoolVars),
	define_order_vars(NOrders, OrderVars).



/**
define_article_vars(+NOrders:Integer, +NArticles:Integer, ArticleVars:IntVars) is det

This predicate defines a list of variables, one for each article in the SORT Box.
An article variable contains the order indices plus the values -1 and 0.
It takes as input:
* an integer NOrders that defines the number of orders.
* an integer NArticles that contains the number of articles.
and outputs:
* ArticleVars a list of integer variables whose domain contains the order indices
  plus the values StayDestination and GoBackDestination (these values can be set
  with the predicate sort_setting:set_setting/2).
*/
define_article_vars(NOrders, NArticles, ArticleVars) :-
  length(ArticleVars, NArticles),
  sort_setting(go_back_destination, GoBackDestination),
  sort_setting(stay_destination, StayDestination),
  ArticleVars #:: [GoBackDestination, StayDestination, 1 .. NOrders].


/**
define_article_bool_vars(+ArticleVars:IntVars, -ArticleBoolVars:BoolVars)

This predicate defines the list of boolean for articles.
If an article variable has value different than stay of goback (the article is associated with
an order) than the corresponding boolean variable has value 1.
It takes as input:
* ArticleVars, a list of variables.
and outputs:
* ArticleBoolVars a list of boolean variables.
*/
define_article_bool_vars(ArticleVars, ArticleBoolVars) :-
  sort_setting(go_back_destination, GoBackDestination),
  sort_setting(stay_destination, StayDestination),
  (
    foreach(ArticleVar, ArticleVars),
    foreach(Bool, ArticleBoolVars),
    param(GoBackDestination, StayDestination)
  do
    and(ArticleVar #\= GoBackDestination, ArticleVar #\= StayDestination, Bool)
    % ArticleVar #> 0 <=> Bool
  ).

/**
define_order_vars(+NOrders:Integer, -OrderVars:IntVars)

This predicate defines a list of variables, one for each order in the SORT Box.
A variable defines the destination of the associated article.
It takes as input:
* NOrders,  an integer which is the number of orders.
and outputs:
* OrderVars, a list of integer variables from 0 to NDestinations.
*/
define_order_vars(NOrders, OrderVars) :-
  % create the list of variables
  length(OrderVars, NOrders),
  OrderVars #:: 0..1.


box_user_constraints(ArticleIds, ArticleVars) :-
  sort_setting(stay_destination, Stay),
  findall(BoxId, box_to_use(BoxId), BoxIds),
  length(BoxIds, L),
  (L > 0 ->
    findall(ArticleId,
    (article{article_id:ArticleId, box_id:ArticleBoxId}, member(ArticleBoxId, BoxIds)),
    ArticleToUseIds),
    get_articlevars_by_ids(ArticleToUseIds, ArticleIds, ArticleVars, ArticleToUseVars),
    gfd:all_ne(ArticleToUseVars, Stay)
  ;
    % do nothing
    true
  ).

order_user_constraints(_ArticleIds, _ArticleVars, OrderIds, OrderVars) :-
  % sort_setting(stay_destination, Stay),
  findall(_OrderId, order_to_satisfy(_OrderId), OrderToSatisfyIds),
  length(OrderToSatisfyIds, L),
  (L > 0 ->
    get_ordervars_by_ids(OrderToSatisfyIds, OrderIds, OrderVars, OrderToSatisfyVars),
    gfd:all_ne(OrderToSatisfyVars, 0)
  ;
    % do nothing
    true
  ).


set_constraints(ArticleIds, ArticleVars, ArticleBools, OrderIds, OrderVars, _BoxIds, _BoxVars) :-
  % constraints used to reduce the domains of articles and orders
  domain_reduction_constraints(ArticleIds, ArticleVars, OrderIds, OrderVars),
  logger:debugln("defined domain reduction constraints"),
  set_article_constraints(ArticleIds, ArticleVars, ArticleBools, OrderIds, OrderVars),
  logger:debugln("defined article constraints"),
  set_order_constraints(ArticleIds, ArticleVars, ArticleBools, OrderIds, OrderVars),
  logger:debugln("defined order constraints"),
  true.

domain_reduction_constraints(ArticleIds, ArticleVars, OrderIds, OrderVars) :-
  % each article can be associated only with compatible orders
  incompatibility_constraint(ArticleIds, ArticleVars, OrderIds),!,
  % if there are not enough articles the order is unsatisfiable. Used to prune solutions
  unsatisfiable_orders_constraint(OrderIds, OrderVars, UnsatisfiableOrderIds),
  % remove from the domains of articles the unsatisfiable orders
  remove_orders_articles_domain(ArticleVars, UnsatisfiableOrderIds, OrderIds),
  true.

/**
  Removes from the domains of article variables those values that correspond to
  those order which are incompatible
*/
incompatibility_constraint(ArticleIds, ArticleVars, OrderIds) :-
  (
    foreach(ArticleId, ArticleIds),
    foreach(ArticleVar, ArticleVars),
    param(OrderIds)
  do
    get_incompatible_orders_by_article(ArticleId, IncompatibleOrderIds),
    (
      foreach(IncompatibleOrderId, IncompatibleOrderIds),
      param(ArticleVar, OrderIds)
    do
      nth1(Idx, OrderIds, IncompatibleOrderId),
      gfd_vars_exclude([ArticleVar], Idx)
    )
  ).

unsatisfiable_orders_constraint(OrderIds, OrderVars, UnsatisfiableOrderIds) :-
  unsatisfiable_orders(OrderIds, UnsatisfiableOrderIds),
  (
    foreach(UnsatOrderId, UnsatisfiableOrderIds),
    param(OrderVars, OrderIds)
  do
    get_var_by_id(UnsatOrderId, OrderIds, OrderVars, OrderVar),
    OrderVar #= 0
  ).

% remove from the domains of articles the unsatisfiable orders
remove_orders_articles_domain(ArticleVars, UnsatisfiableOrderIds, OrderIds) :-
  (
    foreach(UnsatOrderId, UnsatisfiableOrderIds),
    param(OrderIds, ArticleVars)
  do
    nth1(Idx, OrderIds, UnsatOrderId),
    (
      foreach(ArticleVar, ArticleVars),
      param(Idx)
    do
      ArticleVar #\= Idx
    )
  ).

set_article_constraints(ArticleIds, ArticleVars, ArticleBools, OrderIds, OrderVars) :-
  %
  findall(BoxId, article{box_id:BoxId}, BoxIdList),
  % removes duplicates
  sort(BoxIdList, BoxIdListUniques),
  ( foreach(BoxId, BoxIdListUniques),
    param(ArticleIds, ArticleVars, ArticleBools)
  do
    set_article_constraint_box(ArticleIds, ArticleVars, BoxId),
    % if all the articles in a box in their domains have only destinations "stay" or "go back"
    % then force all the articles to "stay"
    set_article_constraint_stay_box(ArticleIds, ArticleVars, ArticleBools, BoxId)
  ),
  % if there is an article which is associated with an order, then that order must be satisfied.
  set_article_force_order_constraint(ArticleIds, ArticleVars, OrderIds, OrderVars),
  % if an order has destination "stay" there must be no article associated with that order.
  remove_order_constraint(ArticleIds, ArticleVars, OrderIds, OrderVars).

/*
If an article is not associated with "stay", then all the articles in the same
box must have a value different from "stay".
This is because the articles are in a box and it is not possible to use only
one article without opening the whole box and putting all the articles in the sorter.
*/
set_article_constraint_box(ArticleIds, ArticleVars, BoxId) :-
  % SameBoxArticleIds are the article ids of the articles in the same box
  findall(ArticleId, article{article_id:ArticleId, box_id:BoxId}, SameBoxArticleIds),
  get_articlevars_by_ids(SameBoxArticleIds, ArticleIds, ArticleVars, SameBoxArticleVars),
  sort_setting(stay_destination, StayDestination),
  ( foreach(ArticleVar, SameBoxArticleVars),
    foreach(StayBool, ArticleStayBools),
    % param(ArticleVars, ArticleIds, SameBoxArticleIds)
    param(StayDestination, SameBoxArticleVars)
  do
    ArticleVar #= StayDestination <=> StayBool,
    % if at least one article has a value different from "stay" 0,
    % then all the the articles in the same box must have a value different
    % from "stay"
    B2 #= -100 * StayBool,
    all_ne(SameBoxArticleVars, B2)
  ),
  % if there is at least one article should not move ("stay" destination),
  % than all the articles in the same box must not move.
  length(SameBoxArticleIds, NBoxArticles),
  gfd:sum(ArticleStayBools, NArticleStay),
  NArticleStay #> 0 <=> NArticleStay #= NBoxArticles,
  %NArticleStay #= 0 => ZeroOccurrences #= 0,
  %NArticleStay #> 0 => ZeroOccurrences #= NBoxArticles,
  %ZeroOccurrences #= NArticleStay,
  % !! redundant (the constraints above should be enough)!!
  occurrences(StayDestination, SameBoxArticleVars, NArticleStay).


% if all the articles in a box in their domains have only destinations "stay" or "go back"
% then force all the articles to "stay"
set_article_constraint_stay_box(ArticleIds, ArticleVars, ArticleBools, BoxId) :-
  findall(ArticleId, article{article_id:ArticleId, box_id:BoxId}, SameBoxArticleIds),
  get_articlevars_by_ids(SameBoxArticleIds, ArticleIds, ArticleBools, SameBoxArticleBools),
  get_articlevars_by_ids(SameBoxArticleIds, ArticleIds, ArticleVars, SameBoxArticleVars),
  sort_setting(stay_destination, StayDestination),
  length(SameBoxArticleIds, NBoxArticles),
  gfd:sum(SameBoxArticleBools, NUsedArticles),
  Occ :: [0, NBoxArticles],
  NUsedArticles #> 0 <=> Occ #= 0,
  NUsedArticles #= 0 <=> Occ #= NBoxArticles,
  occurrences(StayDestination, SameBoxArticleVars, Occ).

/**
If there is an article which is associated with an order, then that order must be satisfied.

Prendo gli ordini compatibili con un articolo e impongo che se la destinazione dell'articolo
è > 0 allora almeno un ordine compatibile deve avere destinazione > 0
*/
set_article_force_order_constraint(ArticleIds, ArticleVars, OrderIds, OrderVars) :-
  ( foreach(ArticleId, ArticleIds),
    param(ArticleIds, ArticleVars, OrderIds, OrderVars)
  do
    get_var_by_id(ArticleId, ArticleIds, ArticleVars, ArticleVar),
    % select compatible orders
    get_compatible_orders_by_article(ArticleId, CompatibleOrderIds),
    %get_ordervars_by_ids(CompatibleOrderIds, OrderIds, OrderVars, CompatibleOrderVars),
    (
      foreach(CompatibleOrderId, CompatibleOrderIds),
      %foreach(CompatibleOrderVar, CompatibleOrderVars),
      param(OrderIds, OrderVars, ArticleVar)
    do
      nth1(Idx, OrderIds, CompatibleOrderId),
      get_var_by_id(CompatibleOrderId, OrderIds, OrderVars, OrderVar),
      ArticleVar #= Idx => OrderVar #> 0
    )
  ).

/**
  If an order will not be satisfied, there must be no article associated with that order.

*/
remove_order_constraint(ArticleIds, ArticleVars, OrderIds, OrderVars) :-
  (
    foreach(OrderId, OrderIds),
    foreach(OrderVar, OrderVars),
    param(ArticleIds, ArticleVars, OrderIds)
  do
    nth1(OrderIdx, OrderIds, OrderId),
    get_compatible_articles_by_order(OrderId, CompatibleArticleIds),
    get_articlevars_by_ids(CompatibleArticleIds, ArticleIds, ArticleVars, CompatibleArticleVars),
    Occurrence :: [-100, OrderIdx],
    (OrderVar #> 0 => Occurrence #= -100),
    (OrderVar #= 0 => Occurrence #= OrderIdx),
    occurrences(Occurrence, CompatibleArticleVars, 0)
  ).

set_order_constraints(ArticleIds, ArticleVars, ArticleBools, OrderIds, OrderVars) :-
  % there is a limit of satisfiable orders. This limit is the number of Sorter's outputs
  max_satisfiable_orders(OrderVars),
  % constraint between orders with the same feed category
  %competitive_orders_constraints(ArticleIds, ArticleBools, OrderIds, OrderVars),
  % for each order of the same category, if all the compatible articles are associated
  % with "stay" or "go back" then all the order of the same category have destination "stay"
  stay_order_constraints(ArticleIds, ArticleBools, OrderIds, OrderVars),
  % satisfiability of the order
  order_satisfiability(ArticleIds, ArticleVars, OrderIds, OrderVars),
  true.


/**
  The Sorter has NDest possible destinations. Therefore only NDest orders can be
  satisfied at most.
*/
max_satisfiable_orders(OrderVars) :-
  sort_setting(destinations, NDest),
  atmost(NDest, OrderVars, 1).


competitive_orders_constraints(ArticleIds, ArticleBools, OrderIds, OrderVars) :-
  findall(
    Ids,
    bagof(
      Id,
      D^Q^I^order{ order_id:Id, feed_category:_C, deadline:D, quantity:Q, income:I},
      Ids
    ),
    ListOfListOrderIds
  ),
  (
    foreach(CompetitiveOrderIds, ListOfListOrderIds),
    param(OrderVars, OrderIds, ArticleIds, ArticleBools)
  do
    length(CompetitiveOrderIds, NOrders),
    ( NOrders > 1 ->
        get_ordervars_by_ids(CompetitiveOrderIds, OrderIds, OrderVars, CompetitiveOrderVars),
        CompetitiveOrderIds = [OrderId | _],
        get_compatible_articles_by_order(OrderId, CompatibleArticleIds),
        get_articlevars_by_ids(CompatibleArticleIds, ArticleIds, ArticleBools, CompatibleArticleBools),
        get_order_quantities(CompetitiveOrderIds, OrderQuantities),
        get_article_quantities(CompatibleArticleIds, CompatibleArticleQuantities),
        gfd:sum(CompatibleArticleQuantities * CompatibleArticleBools, AvailableQuantity),
        gfd:scalar_product(OrderQuantities, CompetitiveOrderVars, (#=<), AvailableQuantity)
      ;
        true
    )
  ).

stay_order_constraints(ArticleIds, ArticleBools, OrderIds, OrderVars) :-
  findall(
    Ids,
    bagof(Id, D^Q^I^order{ order_id:Id, feed_category:_C, deadline:D, quantity:Q, income:I}, Ids),
    ListOfListOrderIds),
  % sort_setting(stay_destination, StayDestination),
  (
    foreach(CompetitiveOrderIds, ListOfListOrderIds),
    param(ArticleIds, ArticleBools, OrderIds, OrderVars)
  do
    CompetitiveOrderIds = [OrderId | _],
    get_compatible_articles_by_order(OrderId, CompatibleArticleIds),
    get_ordervars_by_ids(CompetitiveOrderIds, OrderIds, OrderVars, CompetitiveOrderVars),
    get_articlevars_by_ids(CompatibleArticleIds, ArticleIds, ArticleBools, CompatibleArticleBools),
    gfd:sum(CompatibleArticleBools, Sum),
    length(CompetitiveOrderIds, NCompetitiveOrders),
    Sum #= 0 <=> Occ #= NCompetitiveOrders,
    occurrences(0, CompetitiveOrderVars, Occ)
  ).

/**
* This is the main constraint. An order to be satified must have enough articles
*/
order_satisfiability(ArticleIds, ArticleVars, OrderIds, OrderVars) :-
  ( foreach(OrderId, OrderIds),
    param(ArticleIds, ArticleVars, OrderIds, OrderVars)
  do
    get_var_by_id(OrderId, OrderIds, OrderVars, OrderVar),
    % get the desired quantity of the order
    order{order_id:OrderId, quantity:OrderQuantity},
    % get the list of the article that have the same feed category of the order
    get_compatible_articles_by_order(OrderId, SameCategoryArticleIds),
    get_sublist_vars_by_ids(SameCategoryArticleIds, ArticleIds, ArticleVars, FeasibleArticleVars),
    ( foreach(ArticleId, SameCategoryArticleIds),
      foreach(ArticleQuantity, ArticleQuantities)
    do
      article{article_id:ArticleId, quantity:ArticleQuantity}
    ),
    MaxAvailableQuantity is sum(ArticleQuantities),
    (MaxAvailableQuantity >= OrderQuantity ->
      % sort_setting(max_order_surplus_percentage, SurplusPercent),
      % Surplus is integer(round(SurplusPercent * OrderQuantity)),
      % MaxOrderQuantity is OrderQuantity + Surplus,
      sort_setting(max_order_surplus, Surplus),
      MaxOrderQuantity is OrderQuantity + Surplus,
      surplus_satisfiability_constraint(OrderVar, OrderQuantity, MaxOrderQuantity, ArticleQuantities),
      length(ArticleQuantities, ListLength),
      length(BoolArticleVars, ListLength),
      BoolArticleVars :: [0,1],
      gfd:sum(ArticleQuantities * BoolArticleVars, TotalQuantity),
      gfd:sum(BoolArticleVars, BoolSum),
      BoolSum #>= 1 * OrderVar,
      OrderVar #= 0 <=> BoolSum #= 0,
      % get the index of the order (the destination of an article is an order index)
      nth1(OrderIdx, OrderIds, OrderId),
      occurrences(OrderIdx, FeasibleArticleVars, BoolSum),
      TotalQuantity #>= OrderQuantity * OrderVar,
      TotalQuantity #=< MaxOrderQuantity * OrderVar,
      ( foreach(BoolArticle, BoolArticleVars),
        foreach(ArticleVar, FeasibleArticleVars),
        param(OrderIdx)
      do
        (BoolArticle #= 1 <=> ArticleVar #= OrderIdx),
        (ArticleVar #\= OrderIdx <=> BoolArticle #= 0)
      )
    ;
      OrderVar #= 0
    )
  ).


surplus_satisfiability_constraint(OrderVar, OrderQuantity, MaxOrderQuantity, ArticleQuantities) :-
  length(ArticleQuantities, N),
  length(BoolVars,N),
  BoolVars :: [0,1],
  gfd:sum(ArticleQuantities*BoolVars, S),
  S #>= OrderQuantity,
  S #> MaxOrderQuantity => OrderVar #= 0.


/*
This predicate define the profit that must be maximized
*/
define_profit(ArticleIds, ArticleBools, ArticleVars, OrderIds, OrderVars, Profit) :-
  % get order incomes
  get_order_incomes(OrderIds, OrderIncomes),
  gfd:sum(OrderIncomes * OrderVars, TotalIncome),
  sort_setting(destinations, NDestinations),
  findall(E, (nth1(I,OrderIncomes,E), I =< NDestinations), BestOrderIncomes),
  get_article_storage_costs(ArticleIds, ArticleStorageCosts),
  AllArticlesTotalStorageCosts is sum(ArticleStorageCosts),
  Limit is sum(BestOrderIncomes),
  Profit #=< Limit,
  logger:debugln(Limit),
  penalties(ArticleIds, ArticleVars, ArticleBools, OrderIds, OrderVars, TotalPenalty),
  Profit #= TotalIncome - TotalPenalty.

penalties(ArticleIds, ArticleVars, ArticleBools, OrderIds, OrderVars, TotalPenalty) :-
  today_storage_cost_unused_articles_penalty(ArticleIds, ArticleBools, UnusedArticlesPenalty),
  late_order_penalties(OrderIds, OrderVars, LateOrderPenalty),
  %LateOrderPenalty = 0,
  sorting_penalties(ArticleIds, ArticleVars, SortingPenalty),
  TotalPenalty #= UnusedArticlesPenalty + LateOrderPenalty + SortingPenalty.

/**
* Penalty for keeping one more day the unused articles.
*/
today_storage_cost_unused_articles_penalty(ArticleIds, ArticleBools, UnusedArticlesPenalty) :-
  sort_setting(daily_storage_cost, DailyStorageCost),
  length(ArticleBools, NArticles),
  % length(ArticleDailyStorageCosts, NArticles),
  % maplist(=(DailyStorageCost), ArticleDailyStorageCosts),
  (
    foreach(ArticleId, ArticleIds),
    foreach(ArticleDailyStorageCost, ArticleDailyStorageCosts),
    param(DailyStorageCost)
  do
    article{article_id:ArticleId, ean:EAN, quantity:ArticleQuantity},
    item{ean:EAN, quantity:ItemQuantity},
    %ArticleDailyStorageCost is integer(DailyStorageCost * round(ArticleQuantity / ItemQuantity))
    ArticleDailyStorageCost is integer(DailyStorageCost * div(ArticleQuantity, 1000))
  ),
  logger:debugln(write_term(ArticleDailyStorageCosts)),
  gfd:sum(ArticleDailyStorageCosts * ArticleBools, UsedArticlesDailyStorageCost),
  AllArticlesStorageCost is sum(ArticleDailyStorageCosts),
  UnusedArticlesPenalty #= AllArticlesStorageCost - UsedArticlesDailyStorageCost.

% /**
% * Penalty for not meeting order deadlines
% */
% late_order_penalties(OrderIds, OrderVars, LateOrderPenalty) :-
%   sort_setting(late_order_penalty, LOP),
%   (
%     foreach(OrderId, OrderIds),
%     foreach(Penalty, Penalties),
%     % TodayLateOrderPenalty is the penalty that must be added if the order is not satisfied today
%     foreach(TodayPenalty, TodayPenalties),
%     param(LOP)
%   do
%     today(Today),
%     order{order_id:OrderId, deadline:Deadline},
%     date_difference(Deadline, Today, Diff),
%     ( Diff =< 0 ->
%       Penalty is (-Diff) * LOP,
%       TodayPenalty is LOP
%     ;
%       Penalty = 0,
%       TodayPenalty = 0
%     )
%   ),
%   LowerBoundLOP is sum(Penalties),
%   % invert order bool variables Bool #= 1 iff OrderVar #= 0
%   length(OrderVars, NOrders),
%   length(NegOrderVars, NOrders),
%   NegOrderVars :: [0,1],
%   (
%     foreach(OrderBool, OrderVars),
%     foreach(NegOrderBool, NegOrderVars)
%   do
%     OrderBool #\= NegOrderBool
%   ),
%   LateOrderPenalty #= LowerBoundLOP + TodayLateOrderPenalty,
%   gfd:scalar_product(TodayPenalties, NegOrderVars, (#=), TodayLateOrderPenalty ),
%   true.

  /**
  * Penalty for not meeting order deadlines
  */
  late_order_penalties(OrderIds, OrderVars, LateOrderPenalty) :-
    sort_setting(late_order_penalty, LOP),
    (
      foreach(OrderId, OrderIds),
      foreach(Penalty, Penalties),
      % TodayLateOrderPenalty is the penalty that must be added if the order is not satisfied today
      foreach(TodayPenalty, TodayPenalties),
      param(LOP)
    do
      today(Today),
      order{order_id:OrderId, deadline:Deadline},
      date_difference(Deadline, Today, Diff),
      ( Diff =< 0 ->
        Penalty is (-Diff) * LOP + LOP,
        TodayPenalty is LOP
      ;
        Penalty = 0,
        TodayPenalty = 0
      )
    ),
    % invert order bool variables Bool #= 1 iff OrderVar #= 0
    length(OrderVars, NOrders),
    length(NegOrderVars, NOrders),
    NegOrderVars :: [0,1],
    (
      foreach(OrderBool, OrderVars),
      foreach(NegOrderBool, NegOrderVars)
    do
      OrderBool #\= NegOrderBool
    ),
    gfd:scalar_product(Penalties, NegOrderVars, (#=), LateOrderPenalty ),
    true.


sorting_penalties(ArticleIds, ArticleVars, SortingPenalty) :-
  sort_setting(sorting_cost, SortingCost),
  sort_setting(go_back_destination, GoBackDestination),
  (
    foreach(ArticleVar, ArticleVars),
    foreach(ArticleId, ArticleIds),
    foreach(Bool, ArticleSortingBools),
    foreach(ArticleSortingCost, ArticleSortingCosts),
    param(SortingCost, GoBackDestination)
  do
    article{article_id:ArticleId, ean:EAN, quantity:ArticleQuantity},
    item{ean:EAN, quantity:ItemQuantity},
    ArticleSortingCost is integer(SortingCost * div(ArticleQuantity, ItemQuantity)),
    % iff the destination of an article is -1 then the bool var is 1, these bool
    % are collected into ArticleSortingBools
    ArticleVar #= GoBackDestination <=> Bool
  ),
  gfd:scalar_product(ArticleSortingCosts, ArticleSortingBools, (#=), SortingPenalty).


search(ArticleIds, ArticleVars, OrderIds, OrderVars, Profit, Profit) :-
  % get all the search settings
  sort_setting(method_articles, MethodArticleVars),
  sort_setting(select_articles, SelectArticleVars),
  sort_setting(choice_articles, ChoiceArticleVars),
  sort_setting(method_orders, MethodOrderVars),
  sort_setting(select_orders, SelectOrderVars),
  sort_setting(choice_orders, ChoiceOrderVars),
  sort_setting(strategy, Strategy),
  sort_setting(search_style, SearchStyle),
  sort_setting(delta, Delta),
  sort_setting(timeout, Timeout),
  Cost #= -Profit,
  %Cost #< 0,
  logger:debugln(Cost),
  % (
  %   gfd_search:search(OrderVars, 0, SelectOrderVars, ChoiceOrderVars, MethodOrderVars, []),
  %   gfd_search:search(ArticleVars, 0, SelectArticleVars, ChoiceArticleVars, MethodArticleVars, [])
  % ),
  (SearchStyle = separated ->
    bb_min(
      (
        gfd_search:search(OrderVars, 0, SelectOrderVars, ChoiceOrderVars, MethodOrderVars, []),
        gfd_search:search(ArticleVars, 0, SelectArticleVars, ChoiceArticleVars, MethodArticleVars, [])
      ),
      Cost,
      bb_options{
        report_timeout:report_timeout,
        report_failure:report_failure,
        report_success:print_solutions(OrderIds, OrderVars, ArticleIds, ArticleVars),
        timeout:Timeout,
        delta:Delta,
        strategy:Strategy
        }
      )
  ;
    Select = SelectOrderVars,
    Choice = ChoiceOrderVars,
    Method = MethodOrderVars,
    reverse(OrderVars, OrderVarsR),
    append(OrderVarsR,ArticleVars, Vars),
    %reverse(Vars, VarsR),
    bb_min(
      (
        gfd_search:search(Vars, 0, Select, Choice, Method, [])
      ),
      Cost,
      bb_options{timeout:Timeout, delta:Delta, strategy:Strategy}
    )
  ),
  true.


report_timeout(_Cost, _, _) :-
  infoln("Timeout").
  % flush(output).

report_failure(Cost, _, _) :-
  info("Found no solutions with cost "),
  infoln(Cost),
  flush(output).

print_solutions(OrderIds, OrderVars, ArticleIds, ArticleVars, Cost, _, _) :-
  info("Found a solution with cost "),
  infoln(Cost),
  %write_term(OrderVars, [depth(full), nl(true)]),
  % flush(output),
  % get only the satisfied orders
  ( foreach(OrderId, OrderIds),
    foreach(OrderVar, OrderVars),
    fromto(SatisfiedOrderIds,OutOrderIds,InOrderIds,[]),
    fromto(SatisfiedOrderVars,OutOrderVars,InOrderVars,[])
  do
    (OrderVar > 0 ->
      OutOrderIds = [OrderId | InOrderIds],
      OutOrderVars = [OrderVar | InOrderVars]
      ;
      OutOrderIds = InOrderIds,
      OutOrderVars = InOrderVars
    )
  ),
  % get only the used articles
  % UsedArticleOrderIds is the list of the order associated with the articles
  ( foreach(ArticleId, ArticleIds),
    foreach(ArticleVar, ArticleVars),
    fromto(UsedArticleIds, OutArticleIds,InArticleIds,[]),
    fromto(UsedArticleOrderIds, OutArticleOrderIds, InArticleOrderIds, []),
    param(OrderIds)
  do
    (ArticleVar > 0 ->
      OutArticleIds = [ArticleId|InArticleIds],
      % writeln(ArticleVar),
      nth1(ArticleVar, OrderIds, ArticleOrderId),
      OutArticleOrderIds = [ArticleOrderId | InArticleOrderIds]
    ;
      % writeln(ArticleVar),
      OutArticleIds = InArticleIds,
      OutArticleOrderIds = InArticleOrderIds
    )
  ),
  sort_setting(standalone, Standalone),
  Profit is -Cost,
  (Standalone ->
    % write_term(solution(Cost, SatisfiedOrderIds, SatisfiedOrderVars, UsedArticleIds, UsedArticleOrderIds),  [depth(full), nl(true)])
    infoln(solution(Profit, SatisfiedOrderIds, SatisfiedOrderVars, UsedArticleIds, UsedArticleOrderIds))
  ;
    write_exdr(eclipse_to_java, solution(Profit, SatisfiedOrderIds, SatisfiedOrderVars, UsedArticleIds, UsedArticleOrderIds)),
    flush(eclipse_to_java)
  ).


select_income(Var, Elem) :-
    get_domain_as_list(Var, Domain),
    first_gt_zero(Domain, Elem).
