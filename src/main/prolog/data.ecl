:- module(data).

:- use_module(library(cio)).
:- use_module(utils).
:- use_module(sort_setting).
:- use_module(logger).

:- export struct( article(article_id, ean, box_id, expiry, storage_date, feed_category, quantity) ).
:- export struct( order(order_id, feed_category, deadline, quantity, income) ).
:- export struct( item(ean, quantity, volume, feed_category) ).

:- dynamic
    % db_file/1,
    article/7,
    order/5,
    item/4,
    box_to_use/1,
    order_to_satisfy/1.

:- export
    clean_db/0,
    load_db/1,
    set_boxes_to_use/1,
    set_orders_to_satisfy/1,
    save_data/2,
    unsatisfiable_orders/2,
    article/7,
    order/5,
    item/4,
    box_to_use/1,
    order_to_satisfy/1,
    % db_file/1,
    get_compatible_orders_by_article/2,
    get_incompatible_orders_by_article/2,
    incompatible_order_given_article_condition/2,
    get_compatible_articles_by_order/2,
    compatible_article_given_order_condition/2,
    get_order_incomes/2,
    get_order_quantities/2,
    get_article_quantities/2,
    get_article_storage_costs/2.

/**
Assert (with assertz/1) a list of predicates in Prolog's database
*/
assert_predicates(Predicates) :-
  (
    foreach(P, Predicates)
  do
    assertz(P)
  ).

clean_db :-
  retractall(article(_,_,_,_,_,_,_)),
  retractall(order(_,_,_,_,_)),
  retractall(item(_,_,_,_)).

load_db(Dataset) :-
  [Dataset].

set_boxes_to_use(Boxes) :-
  logger:debugln("asserting boxes"),
  retractall(box_to_use(_)),
  assert_predicates(Boxes) @ data.

set_orders_to_satisfy(Orders) :-
  logger:debugln("asserting orders"),
  retractall(order_to_satisfy(_)),
  assert_predicates(Orders) @ data.

save_data(Data, OutputFile) :-
  logger:debugln("asserting data"),
  assert_predicates(Data) @ data,
  logger:debugln("data asserted"),
  logger:debugln("sorting orders by income"),
  findall(order{order_id:OrderId, feed_category:FeedCategory, deadline:Deadline, quantity:Q, income:Income},
    order{order_id:OrderId, feed_category:FeedCategory, deadline:Deadline, quantity:Q, income:Income},
    Orders),
  % sort the list using the 2nd key as criterium
  sort(income of order, >=, Orders, SortedOrders),
  retractall(order(_,_,_,_,_)),
  assert_predicates(SortedOrders),
  atom_string(OutputFileAtom, OutputFile),
  tell(OutputFileAtom),
  listing(article/7),
  listing(order/5),
  listing(item/4),
  told,
  logger:debugln("cleaning previous data"),
  clean_db,
  logger:debugln("cleaning completed").

/**
  Get the list of those orders which are unsatisfiable, i.e. those orders that
  request an amount of feed materials which is not available, and constraints the
  values of the unsatisfiable orders to "stay" destination.
*/
unsatisfiable_orders(OrderIds, UnsatisfiableOrderIds) :-
  % sort_setting(stay_destination, Stay),
  ( foreach(OrderId, OrderIds),
    fromto(UnsatisfiableOrderIds,Out,In,[])
    % param(OrderVars, OrderIds)
  do
    % get_var_by_id(OrderId, OrderIds, OrderVars, OrderVar),
    % get the desired quantity of the order
    order{order_id:OrderId, quantity:OrderQuantity},
    % get the list of the article quantities compatible with the order
    get_compatible_articles_by_order(OrderId, CompatibleArticleIds),
    get_article_quantities(CompatibleArticleIds, ArticleQuantities),
    Sum is sum(ArticleQuantities),
    (Sum < OrderQuantity ->
      % OrderVar #= 0,
      Out=[OrderId|In]
      ;
      Out=In
    )
  ).

get_compatible_orders_by_article(ArticleId, CompatibleOrderIds) :-
  article{article_id:ArticleId, ean:EAN, expiry:ArticleExpiry},
  today(TodayDate),
  date_difference(ArticleExpiry, TodayDate, Diff),
  % check if the article is not expired (at least one day before expiration)
  ( Diff > 0 ->
    item{ean:EAN, feed_category:FeedCategory},
    findall(
      OrderId,
      (order{order_id:OrderId, feed_category:OrderFeedCategory},
      OrderFeedCategory = FeedCategory
      % (
      %   OrderFeedCategory = FeedCategory
      % ;
      %   OrderFeedCategory = '0')
      ),
      CompatibleOrderIds)
  ;
    % if the article is expired only the orders with feed category '0' are compatible
    findall(OrderId, order{order_id:OrderId, feed_category:'0'}, CompatibleOrderIds)
  ).

get_incompatible_orders_by_article(ArticleId, IncompatibleOrderIds) :-
  article{article_id:ArticleId, ean:EAN, expiry:ArticleExpiry},
  today(TodayDate),
  date_difference(ArticleExpiry, TodayDate, Diff),
  % check if the article is not expired
  ( Diff > 0 ->
    item{ean:EAN, feed_category:ArticleFeedCategory},
    findall(
      OrderId,
      incompatible_order_given_article_condition(OrderId, ArticleFeedCategory),
      IncompatibleOrderIds)
  ;
    % if the article is expired all the orders with feed category different from '0'
    % are incompatible
    findall(OrderId,
      (order{order_id:OrderId, feed_category:OrderFeedCategory},
      OrderFeedCategory \= '0'),
      IncompatibleOrderIds)
  )
  .

% conditions for an order to be incompatible given a not expired article
incompatible_order_given_article_condition(OrderId, ArticleFeedCategory) :-
  order{order_id:OrderId, feed_category:OrderFeedCategory},
  % OrderFeedCategory \= '0',
  (
    OrderFeedCategory = '0'
  ;
    ArticleFeedCategory \= OrderFeedCategory
  ).
  % (OrderFeedCategory = '0' ->
  %   false
  % ;
  %   date_difference(ArticleExpiry, OrderDeadline, Diff),
  %   (
  %     ArticleFeedCategory \= OrderFeedCategory
  %   ;
  %     Diff =< 0
  %   )
  % ).


get_compatible_articles_by_order(OrderId, CompatibleArticleIds) :-
  order{order_id:OrderId, feed_category:FeedCategory},
  % (FeedCategory \= '0' ->
  %   findall(
  %     ArticleId,
  %     (
  %     % item{ean:EAN, feed_category:FeedCategory},
  %     % article{article_id:ArticleId, ean:EAN, expiry:ArticleExpiry, feed_category:FeedCategory},
  %     article{article_id:ArticleId, expiry:ArticleExpiry, feed_category:FeedCategory},
  %     % check if article is expired
  %     today(TodayDate),
  %     date_difference(ArticleExpiry, TodayDate, Diff),
  %     Diff > 0),
  %     % compatible_article_given_order_condition(ArticleId, FeedCategory),
  %     CompatibleArticleIds)
  % ;
  %   findall(ArticleId, article{article_id:ArticleId}, CompatibleArticleIds)
  % ).
  findall(ArticleId, compatible_article_given_order_condition(ArticleId, FeedCategory), CompatibleArticleIds).

  /* if the order feed category is different from '0', then take all the non expired
  article which are compatible. Otherwise take all the expired articles
  */
compatible_article_given_order_condition(ArticleId, OrderFeedCategory) :-
  (OrderFeedCategory \= '0' ->
    item{ean:EAN, feed_category:OrderFeedCategory},
    article{article_id:ArticleId, ean:EAN, expiry:ArticleExpiry},
    % check if article is expired
    today(TodayDate),
    date_difference(ArticleExpiry, TodayDate, Diff),
    % at least one day before expiry
    Diff > 0
  ;
    article{article_id:ArticleId, expiry:ArticleExpiry},
    % check if article is expired
    today(TodayDate),
    date_difference(ArticleExpiry, TodayDate, Diff),
    % if it is expired can be used with orders with feed category equals to '0'
    Diff =< 0
  ).

get_order_incomes(OrderIds, OrderIncomes) :-
  %sort_setting(euro_factor, Factor),
  (
    foreach(OrderId, OrderIds),
    foreach(_Income, OrderIncomes)
    %param(Factor)
  do
    order{order_id:OrderId, income:Income},
    % _Income is integer(round(Income*Factor))
    _Income is integer(round(Income))
  ).

get_order_quantities(OrderIds, OrderQuantities) :-
  (
    foreach(OrderId, OrderIds),
    foreach(Quantity, OrderQuantities)
  do
    order{order_id:OrderId, quantity:Quantity}
  ).

get_article_quantities(ArticleIds, ArticleQuantities) :-
  (
    foreach(ArticleId, ArticleIds),
    foreach(Quantity, ArticleQuantities)
  do
    article{article_id:ArticleId, quantity:Quantity}
  ).

get_article_storage_costs(ArticleIds, ArticleStorageCosts) :-
  sort_setting(daily_storage_cost, DailyStorageCost),
  (
    foreach(ArticleId, ArticleIds),
    foreach(ArticleStorageCost, ArticleStorageCosts),
    param(DailyStorageCost)
  do
    article{article_id:ArticleId, storage_date:StorageDate, quantity:Quantity},
    % get the days since the article was stored
    days_to_today(StorageDate, NDays),
    ArticleStorageCost is integer(DailyStorageCost * div(Quantity, 1000) * NDays)
  ).
