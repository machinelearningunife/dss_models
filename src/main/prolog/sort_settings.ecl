sort_setting(destinations, 8).

% max surplus in grams
sort_setting(max_order_surplus, 5000).

% the storage cost for a kilogram of article (integer), it corresponds to 5 * 10^{-4} euros
sort_setting(daily_storage_cost, 5).

% the cost for sorting an article (integer), it corresponds to 5 * 10^{-4} euros
sort_setting(sorting_cost, 1000).

sort_setting(late_order_penalty, 10000).

sort_setting(timeout, 0).

sort_setting(method_articles, complete).

sort_setting(select_articles, select_income).

sort_setting(choice_articles, indomain_max).

sort_setting(method_orders, complete).

sort_setting(select_orders, select_income).

sort_setting(choice_orders, indomain_max).

sort_setting(strategy, continue).

sort_setting(search_style, separated).

sort_setting(delta, 1).

sort_setting(solver, clp_engine).

sort_setting(standalone, false).

sort_setting(logger, info).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WARNING: do not change the following settings %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sort_setting(stay_column, 2).
sort_setting(go_back_column, 1).
% value of the 'stay' destination
sort_setting(stay_destination, 0).
% value of the 'go back' destination
sort_setting(go_back_destination, -1).
%
%sort_setting(euro_factor, 10000)
