:- module(logger).

:- use_module(sort_setting).

:- export
    fatal/1,
    fatalln/1,
    error/1,
    errorln/1,
    warn/1,
    warnln/1,
    info/1,
    infoln/1,
    debug/1,
    debugln/1,
    trace/1,
    traceln/1.

% OFF, FATAL, ERROR, WARN, INFO, DEBUG, TRACE

level(off, 0).
level(fatal, 1).
level(error, 2).
level(warn, 3).
level(info, 4).
level(debug, 5).
level(trace, 6).


log(listing(Predicate), Level) :- !,
  level(Level, MessageLevel),
  sort_setting(logger, SettingLevel),
  level(SettingLevel, SettingLevelNum),
  (MessageLevel =< SettingLevelNum ->
    call(listing(Predicate))
  ;
    true
  ).

log(listing(Predicate) @ Module, Level) :- !,
  level(Level, MessageLevel),
  sort_setting(logger, SettingLevel),
  level(SettingLevel, SettingLevelNum),
  (MessageLevel =< SettingLevelNum ->
    call(listing(Predicate) @ Module)
  ;
    true
  ).

log(Message, Level, NL) :-
  level(Level, MessageLevel),
  sort_setting(logger, SettingLevel),
  level(SettingLevel, SettingLevelNum),
  (MessageLevel =< SettingLevelNum ->
    write_term(Message, [depth(full), nl(NL), flush(true)])
  ;
    true
  ).


/**
 * fatl(+Message:term) is det
 *
 * The predicate calls log(Message, Level) to print the message
 */
fatal(Message) :-
  log(Message, fatal, false).

fatalln(Message) :-
  log(Message, fatal, true).

error(Message) :-
  log(Message, error, false).

errorln(Message) :-
  log(Message, error, true).

warn(Message) :-
  log(Message, warn, false).

warnln(Message) :-
  log(Message, warn, true).

info(Message) :-
  log(Message, info, false).

infoln(Message) :-
  log(Message, info, true).

debug(Message) :-
  log(Message, debug, false).

debugln(Message) :-
    log(Message, debug, true).

trace(Message) :-
  log(Message, trace, false).

traceln(Message) :-
    log(Message, trace, true).
