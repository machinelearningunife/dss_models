:- module(utils).

:- use_module(library(listut)).
:- use_module(library(calendar)).
%:- dynamic var_index/2.
%:- export assign_indices/1, var_index/2.
%:- export assign_indices/1.
:- export
    get_ordervars_by_ids/4,
    get_articlevars_by_ids/4,
    get_sublist_vars_by_ids/4,
    get_var_by_id/4,
    days_to_today/2,
    date_difference/3,
    today/1,
    first_gt_zero/2.


/*
Gets all the order destination vars corresponding to the given list of identifiers
(relies on a previous assertion of
var_index)
*/
get_ordervars_by_ids(SublistOrderIds, OrderIds, OrderVars, SublistOrderVars) :-
  get_sublist_vars_by_ids(SublistOrderIds, OrderIds, OrderVars, SublistOrderVars).

/*
Gets all the article destination variables corresponding to the given list of
identifiers (relies on a previous assertion of
var_index)
*/
get_articlevars_by_ids(ArticleSubIds, ArticleIds, ArticleVars, SublistArticleVars) :-
  get_sublist_vars_by_ids(ArticleSubIds, ArticleIds, ArticleVars, SublistArticleVars).

get_var_by_id(Id, Ids, Vars, Var) :-
  nth0(Idx, Ids, Id),!,
  nth0(Idx, Vars, Var).

/**
get_sublist_vars_by_ids(+Ids:List, +Vars:VarList, -SublistVars:VarList)
*/
get_sublist_vars_by_ids(SubIds, Ids, Vars, SublistVars) :-
    (
      foreach(Id, SubIds),
      foreach(Var, SublistVars),
      param(Vars, Ids)
      do
      get_var_by_id(Id, Ids, Vars, Var)
    ).
    /*
  (
    foreach(Id, Ids),
    foreach(Var, SublistVars),
    param(Vars)
    do
    get_var_by_id(Id, Vars, Var)
  ).
  */

 % today(TodayDate) :-
 %   mjd_now(_MJDNow),
 %   mjd_to_date(_MJDNow, TodayDate).

today(01/06/2020).

/*
* This predicate return the difference between Date1 and Date2
*/
date_difference(Date1, Date2, Diff) :-
  % convert dates into julian day
  atomic_term(Date1, Date1Term),
  atomic_term(Date2, Date2Term),
  date_to_mjd(Date1Term, MJD1),
  date_to_mjd(Date2Term, MJD2),
  Diff is MJD1 - MJD2.

/*
This predicate computes how many days have been passed until today.
*/
days_to_today(Date, Days) :-
  % convert dates into julian day
  atomic_term(Date, DateTerm),
  date_to_mjd(DateTerm, MJD),
  today(TodayDate),
  date_to_mjd(TodayDate, MJDToday),
  Days is MJDToday - MJD.

/*
* first_gt_zero unifies Elem with the first element of a list greater than 0, if it exists.
  Otherwise it unifies Elem with 0
*/
first_gt_zero([], 0).

first_gt_zero([H |_T], H) :- H > 0, !.

first_gt_zero([_H|T], Elem) :-
  first_gt_zero(T, Elem).

atomic_term(Atomic, Term) :-
  ( atomic(Atomic) ->
    atomics_to_string([Atomic], String),
    term_string(Term, String)
  ;
    Term = Atomic
  ).
