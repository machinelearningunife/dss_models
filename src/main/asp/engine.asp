#const noutput = 8.
noutput(noutput).

#const max_order_surplus = 5000.
max_order_surplus(max_order_surplus).

#const storage_cost = 5.
storage_cost(storage_cost).

#const sorting_cost = 1000.
sorting_cost(sorting_cost).

#const late_order_penalty = 10000.
late_order_penalty(late_order_penalty).

%#const euro_factor = 10000.
%euro_factor(euro_factor).

% order(Order_id, Feed_category, Deadline, Quantity, Income).

item(Ean, ItemQuantity, ItemCategory) :- item(Ean, ItemQuantity, ItemVolume, ItemCategory).

0 { satisfied_order(Order_id,Income): order(Order_id, Feed_category, Deadline, Quantity, Income) } N :- noutput(N).

satisfied_order(Order_id) :- satisfied_order(Order_id,Income).

% article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, Quantity).

{ article_used(Article_id,Order_id) } :-
    article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity),
    order(Order_id, Feed_category, Deadline, OrderQuantity, Income).

article_used(Article_id) :- article_used(Article_id,Order_id).

article_unused(Article_id) :- article_back(Article_id).
article_unused(Article_id) :- article_stay(Article_id).

:- not article_used(Article_id),
  not article_unused(Article_id),
  article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity).

{ article_back(Article_id) } :- article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity).

{ article_stay(Article_id) } :- article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity).

:- article_stay(ArticleId), article_back(ArticleId).
:- article_stay(ArticleId), article_used(ArticleId).
:- article_back(ArticleId), article_used(ArticleId).


box(Box_id) :- article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity).

%{ open_box(Box_id) } :- box(Box_id).
open_box(Box_id) :- article_used(Article_id,Order_id), article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity).
open_box(Box_id) :- article_back(Article_id), article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity).

:- article_stay(Article_id), article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity), open_box(Box_id).

:- article_used(ArticleId,OrderId1), article_used(ArticleId,OrderId2), OrderId1 != OrderId2.

% (17)

box_used_in_order(Box_id) :- article_used(Article_id,Order_id), article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity).

:- open_box(Box_id), not box_used_in_order(Box_id).

% (18)

:- not satisfied_order(Order_id), article_used(Article_id,Order_id).

% (20)
:- satisfied_order(Order_id),
   order(Order_id, Feed_category, Deadline, OrderQuantity, Income),
   #sum { ArtQuantity, Article_id :  article_used(Article_id, Order_id), article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity)  } < OrderQuantity.

% (21)

:- order(Order_id, Feed_category, Deadline, OrderQuantity, Income),
   max_order_surplus(Surplus),
   satisfied_order(Order_id),
   % article_used(Article_id,Order_id),
   #sum { ArtQuantity, Article_id :  article_used(Article_id, Order_id), article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity)  } > OrderQuantity + Surplus.

% Il (22) per ora non lo mettiamo, pensare se vale la pena metterlo.

#maximize { Income,Order_id : satisfied_order(Order_id, Income);
%            -ArticleStorageCost,Article_id : storage_cost(StorageCost), article_unused(Article_id), article(Article_id, Ean, _, _, _, _, ArtQuantity), item(Ean, ItemQuantity, _), ArticleStorageCost = (ArtQuantity / ItemQuantity) * StorageCost;
            -ArticleStorageCost,Article_id : storage_cost(StorageCost), article_unused(Article_id), article(Article_id, Ean, _, _, _, _, ArtQuantity), item(Ean, ItemQuantity, _), ArticleStorageCost = ArtQuantity / 1000 * StorageCost;
            -ArticleSortingCost,Article_id : sorting_cost(SortingCost), article_back(Article_id), article(Article_id, Ean, _, _, _, _, ArtQuantity), item(Ean, ItemQuantity, _), ArticleSortingCost = (ArtQuantity / ItemQuantity) * SortingCost;
            -LateDaysCost,Order_id : late_order_penalty(LateCost), not satisfied_order(Order_id), order(Order_id, Feed_category, Deadline, Quantity, Income), LateDaysCost = @late_days(Deadline) * LateCost
    }.


% cost(C) :-
%   #sum {
%     ArticleStorageCost,Article_id :
%       storage_cost(StorageCost),
%       article_unused(Article_id),
%       article(Article_id, Ean, _, _, _, _, ArtQuantity),
%       item(Ean, ItemQuantity, _),
%       ArticleStorageCost = (ArtQuantity / ItemQuantity) * StorageCost
%   } = TotalStorageCost,
%   #sum {
%     ArticleSortingCost,Article_id :
%       sorting_cost(SortingCost),
%       article_back(Article_id),
%       article(Article_id, Ean, _, _, _, _, ArtQuantity),
%       item(Ean, ItemQuantity, _),
%       ArticleSortingCost = (ArtQuantity / ItemQuantity) * SortingCost
%   } = TotalSortingCost,
%     C = TotalStorageCost + TotalSortingCost.
%
% late_cost(LateOrderPenalty) :-
%   #sum {
%     LateDaysCost,Order_id : late_order_penalty(LateCost),
%       not satisfied_order(Order_id),
%       order(Order_id, _, Deadline, _, Income),
%       LateDaysCost = @late_days(Deadline) * LateCost
%       } = LateOrderPenalty.

% total(T) :-
%   #sum {Income,Order_id : satisfied_order(Order_id, Income)} = TotalIncome,
  % #sum {StorageCost,_Article_id : storage_cost(StorageCost), article_unused(_Article_id)} = TotalStorageCost,
  % #sum {ArticleSortingCost,Article_id : sorting_cost(SortingCost),
  %   article_back(Article_id),
  %   article(Article_id, Ean, _, _, _, _, ArtQuantity),
  %   item(Ean, ItemQuantity, _),
  %   ArticleSortingCost = (ArtQuantity / ItemQuantity) * SortingCost
  %   } = TotalSortingCost,
  %   % T = TotalIncome - TotalStorageCost - TotalSortingCost.

#show.
#show satisfied_order/2.
%#show article_used/2.
%#show article_back/1.
%#show article_stay/1.
% #show open_box/1.
% #show cost/1.
% #show late_cost/1.
%#show sum_quantity_order/2.

%#heuristic satisfied_order(Order_id,Income) :
%   order(Order_id, Feed_category, Deadline, OrderQuantity, Income). [Income, true]

% #heuristic satisfied_order(Order_id,Income) :
%   order(Order_id, Feed_category, Deadline, OrderQuantity, Income),
%   #sum { ArtQuantity, Article_id :  article_used(Article_id, Order_id), article(Article_id, _, _, _, _, Feed_category, ArtQuantity)  } > OrderQuantity. [-Income, level]

%#heuristic satisfied_order(Order_id,Income) :
%    order(Order_id, Feed_category, Deadline, OrderQuantity, Income),
%    #sum { ArtQuantity, Article_id :  article_used(Article_id, Order_id), article(Article_id, _, _, _, _, Feed_category, ArtQuantity)  } > OrderQuantity. [@5, false]

% #heuristic satisfied_order(Order_id,Income) :
%   order(Order_id, Feed_category, Deadline, OrderQuantity, Income),
%   #sum { ArtQuantity, Article_id :  article_used(Article_id, Order_id), article(Article_id, _, _, _, _, Feed_category, ArtQuantity)  } < OrderQuantity. [15, false]
%#heuristic article_used(Article_id,Order_id). [100,init]

% #heuristic article_used(Article_id) :
%   article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity),
%   not order(_, Feed_category, _, _, _). [5,false]

% #heuristic article_used(Article_id) :
%     article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity). [20, init]

% #heuristic article_used(Article_id) :
%     article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity),
%     not order(_, Feed_category, _, _, _). [20@10, false]

% #heuristic article_back(Article_id). [1, init]
%#heuristic article_stay(Article_id). [10, true]

% #heuristic article_back(Article_id) :
%     article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity),
%     not order(_, Feed_category, _, _, _). [5,false]
%
% #heuristic article_stay(Article_id) :
%      article(Article_id, Ean, Box_id, Expiry, Storage_date, Feed_category, ArtQuantity),
%      not order(_, Feed_category, _, _, _). [5, init]
